# the files to consider for pep8 and pylint
# ignoring tests/ for now
PYTHON-SOURCES = $(wildcard minisim/*.py)

# check as many things as possivle before committing 
check: pep8 pylint doc test

# trigger the tests
test:
	py.test --cov minisim

test-clean:
	rm -rf .coverage

# run pylint
pylint:
	pylint -f parseable $(PYTHON-SOURCES)

# run pep8 
pep8:
	pep8 $(PYTHON-SOURCES)

# make doc: forward to 'make html' in doc/
doc:
	$(MAKE) --directory=doc html
.PHONY: doc

# update minisim/version.py after an addition in CHANGELOG.md
version:
	python3 tools/update-version-from-changelog
