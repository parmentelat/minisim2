# Changelog

## 0.1.3 - 2017 03 27

* added a webhook to rebuild documentation at readthedocs

## 0.1.2 - 2017 03 26

* a few docstrings added for demonstrating the numpy style
* also 2 samples are added (numpy-style and google-style) to the doc
* at this point we can get readthedocs.io to show our documentation
  at minisim2.readthedocs.io but it is not automatically refreshed
  at each commit, I need to go in readthedocs and trigger a build manually
* it looks like to fix that, I still need to create a dedicated webhook
  in gitlab (which btw is not needed when dealing with a github project)
  

## 0.1.1 - 2017 03 25

* simulation method should be OK - except maybe for units
* a single testcase illustrates how to share globals among tests
* gitlab-ci has 3 jobs in 2 stages
  * raincheck : does pylint and pep8
  * python27 and python35: runs the tests under py2 and py3
* documentation skeleton should be fine, although
  * this is not yet connected to readthedocs, and
  * docstrings are mostly empty for now
* realistic .gitignore
* tools/update-version-from-changelog
  can update `minisim.version.__version__` from `CHANGELOG.md`


## 0.1.0 - 2017 03 23

* initial release
