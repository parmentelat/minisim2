# -*- coding: utf-8 -*-

# unused - thanks pylint
# import numpy as np
import pandas as pd


class Zone(object):
    """
    A zone object corresponds to a closed area, possibly ventilated, and
    with an infiltration that describes its natural insulation leaks
    """

    def __init__(self, volume, infiltration,
                 ventilations=None, occupants=None):
        self.volume = volume
        self._infiltration = infiltration
        self.occupants = set() if occupants is None else set(occupants)
        # when the line becomes too long, may need a backslash
        self.ventilations = set() if ventilations is None\
            else set(ventilations)

    def __repr__(self):
        return "Zone({}, with {} occupants, {} ventilations)"\
            .format(self.infiltration,
                    len(self.occupants),
                    len(self.ventilations))

    # the infiltration property
    def get_infiltration(self):
        if self._infiltration is None:
            raise RuntimeError("Zone has no infiltration")
        else:
            return self._infiltration

    def set_infiltration(self, infiltration):
        if infiltration is self._infiltration:
            return
        if self._infiltration is not None:
            raise RuntimeError("Zone already has an infiltration")
        self._infiltration = infiltration

    infiltration = property(get_infiltration, set_infiltration)

    # managing the other 2 categories of components
    def add_occupants(self, *occupants):
        self.occupants |= set(occupants)

    def remove_occupants(self, *occupants):
        self.occupants -= set(occupants)

    def add_ventilations(self, *ventilations):
        self.ventilations |= set(ventilations)

    def remove_ventilations(self, *ventilations):
        self.ventilations -= set(ventilations)

    #
    def simulate(self, date_range, weather, co2_initial):

        # xxx should be weather.get(date_initial)
        co2_initial = co2_initial or weather.co2_initial

        simulation = pd.DataFrame(index=date_range)
        simulation['co2'] = co2_initial

        print("simulation")
        print(simulation)

        # we can only do one step fewer than the nuber of instants
        for instant in date_range[:-1]:
            previous = simulation.co2[instant]
            outside_co2 = weather.get_co2(instant)
            delta = 0.

            # infiltration from outdoor
            delta += self.infiltration.get_rate(instant) / \
                self.volume * outside_co2
            # infiltration outwards
            delta -= self.infiltration.get_rate(instant) / \
                self.volume * previous

            for ventilation in self.ventilations:
                # outwards
                delta += ventilation.get_rate(instant) / \
                    self.volume * outside_co2
                # inwards
                delta -= ventilation.get_rate(instant) / self.volume * previous

            for occupant in self.occupants:
                # exhale
                delta += occupant.get_rate(instant) / \
                    self.volume * occupant.concentration
                # inhale
                delta -= occupant.get_rate(instant) / self.volume * previous

            # apply that delta
            simulation.co2.loc[instant + 1] = previous + delta

        return simulation
