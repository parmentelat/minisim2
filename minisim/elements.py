# -*- coding: utf-8 -*-

# see http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html
# for more on docstrings

"""
Various classes of elements that can be involved in a simulation

Notes
-----
    As of this release, units for the `rate` attribute are quite fuzzy
    and will need to be reviewed

Also we do not use type hints at all here since they are supported
from 3.5 only and so this would break any attempt to run 2.7

Example
-------

    ``john = Occupant("John", 20)``

"""


class Element(object):
    """
    A generic class for the various objects we will deal with
    as part of a simulation
    """
    # temporarily model all elements with a name and a constant rate

    def __init__(self, name, rate):
        """
        Parameters
        ----------
        name : str
            the name of that element
        rate : float
            its emission rate - I am under the impression that
            the unit here is not quite clear and may vary from element
            to element
        """
        self.name = name
        self.rate = rate

    def __repr__(self):
        return "{} elt (rate={:.2f} ㎥/s)".format(self.name, self.rate)


class Occupant(Element):

    """
    Models a human person in the zone
    """

    def __init__(self, name, rate, concentration=36000):
        """
        An element with an optional concentration
        """
        self.concentration = concentration
        Element.__init__(self, name, rate)

    def get_rate(self, when):
        """
        Parameters
        ----------
        when : pandas.tslib.Timestamp
            at what time

        Returns
        -------
        float
            the rate at that time; this is designed so we can later on
            implement e.g. sleeping hours or similar changes in behaviour
        """

        # xxx this should use self.concentration..
        return self.rate * 600.


class Infiltration(Element):

    def get_rate(self, when):
        return self.rate * 600.


class Ventilation(Element):
    pass
