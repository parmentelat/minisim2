# minisim2

## Status
[![build status](https://gitlab.com/parmentelat/minisim2/badges/master/build.svg)](https://gitlab.com/parmentelat/minisim2/commits/master)
[![coverage report](https://gitlab.com/parmentelat/minisim2/badges/master/coverage.svg)](https://gitlab.com/parmentelat/minisim2/commits/master)
[![Documentation Status](https://readthedocs.org/projects/minisim2/badge/?version=latest)](http://minisim2.readthedocs.io/en/latest/?badge=latest)

## Propos
Un micro simulateur d'émissions de CO2 pour une classe de programmation

## Contenu

* `./minisim/` - le code du package minisim
* `./tests/` - le répertoire où sont rassemblés les tests
* `./doc/source` - le source de la documentation
* `./setup.py` - pour `setuptools`
* `.gitlab-ci.yml` - pour l'intégration continue sur les runners de gitlab
* `.gitignore` - pour que git ignore certaines familles de fichier
* `./minisim.ipynb` : un notebook qui démontre une utilisation possible de minisim

* *divers textes*: ce `README.md`, `CHANGELOG.md`, la licence etc...

* `doc_samples/` - contient deux exemples de docstrings (numpy style et google style)
  qui sont incorporés dans la doc à titre d'illustration

* `*.png` - des copies d'écran pour les réglages que j'ai fait sous `gitlab.com`

## Bookmarks

* [http://minisim2.readthedocs.org/](http://minisim2.readthedocs.org/)
  la documentation du projet remise à jour à chaque push sur gitlab.com
