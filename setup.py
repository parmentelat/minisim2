from setuptools import setup

from minisim import __version__

try:
    with open("README.md") as readme:
        long_description = readme.read()
except Exception as e:
    long_description = "no description could be found"

setup(
    name = 'minisim',
    version = __version__,
    author = "Guillaume Ansanay-Alex, Antoine Breitwiller",
    author_email = "guillaume.ansanay@cstb.fr",
    packages = ['minisim'],
    url = "https://gitlab.com/gansanay/minisim",
    license = "See LICENSE",
    description = "A micro simulator for a programming class",
    long_description = long_description,
    install_requires = [
        'numpy', 'pandas',
    ],
    setup_requires = ['pytest-runner'],
    tests_require = ['pytest', 'pytest-cov', 'numpy', 'pandas'],

    # see https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5', 
    ],
)
