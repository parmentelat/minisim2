import pandas as pd
from minisim import Infiltration, Occupant, Ventilation, Zone, Weather

import pytest

# using an instance to pass global-like variables among tests
class Vars: pass

@pytest.fixture
def test_variables():

    # zone pieces
    volume = 50
    insulated = Infiltration("insulated", 0)
    infiltration1 = Infiltration("infiltration1", 5)
    infiltration2 = Infiltration("infiltration2", 10)
    bob = Occupant('Bob', 20, 36000)
    bob2 = Occupant('Bob', 10)
    mary = Occupant ('Mary', 5)
    dilbert = Occupant('Dilbert', 15)
    ven1 = Ventilation("fan1", 8)
    ven2 = Ventilation("fan2", 12)

    # simulation params
    weather1 = Weather(400)
    weather2 = Weather(800)
    hourly_2hours = pd.date_range('1/1/2017', periods = 2, freq = 'H')
    hourly_2days  = pd.date_range('1/1/2017', periods = 2*24, freq = 'H')
    hourly_2weeks = pd.date_range('1/1/2017', periods = 2*7*24, freq = 'H')
    hourly_2months = pd.date_range('1/1/2017', periods = 2*30*24, freq = 'H')

    # store all locals as attributes in the vars instance
    vars = Vars()
    vars.__dict__ = locals()
    return vars
