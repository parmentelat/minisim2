import pandas as pd
from minisim import Infiltration, Occupant, Ventilation, Zone, Weather

import pytest


def test_infiltration(test_variables):
    vars     = test_variables
    volume   = vars.volume
    inf1     = vars.infiltration1
    inf2     = vars.infiltration2
    
    zone = Zone(volume, None)
    with pytest.raises(RuntimeError):
        zone.infiltration
    zone.infiltration = inf1
    assert zone.infiltration is inf1
    with pytest.raises(RuntimeError):
        zone.infiltration = inf2
    zone.infiltration = inf1


def test_occupants(test_variables):
    vars = test_variables
    volume  = vars.volume
    bob     = vars.bob
    bob2    = vars.bob2
    mary    = vars.mary
    dilbert = vars.dilbert

    zone = Zone(volume, None)
    assert len(zone.occupants) == 0
    zone.add_occupants(mary)
    assert len(zone.occupants) == 1
    zone.add_occupants(dilbert, dilbert)
    assert len(zone.occupants) == 2
    zone.add_occupants(bob, bob2)
    assert len(zone.occupants) == 4
    zone.add_occupants(bob, bob2, mary, dilbert)
    assert len(zone.occupants) == 4
    zone.remove_occupants(bob, bob2, mary, bob2)
    assert len(zone.occupants) == 1
    zone.remove_occupants(bob, bob2, mary, bob2, dilbert)
    assert len(zone.occupants) == 0    


def test_ventilations(test_variables):
    vars = test_variables
    volume = vars.volume
    ven1   = vars.ven1
    ven2   = vars.ven2

    zone = Zone(volume, None)
    assert len(zone.ventilations) == 0
    zone.add_ventilations(ven1)
    assert len(zone.ventilations) == 1
    zone.add_ventilations(ven1)
    assert len(zone.ventilations) == 1
    zone.add_ventilations(ven2)
    assert len(zone.ventilations) == 2
    zone.add_ventilations(ven2)
    assert len(zone.ventilations) == 2
    zone.remove_ventilations(ven1, ven2)
    assert len(zone.ventilations) == 0

