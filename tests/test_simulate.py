import pandas as pd
from minisim import Infiltration, Occupant, Zone, Weather

import pytest

# test_variables is defined in conftest.py
# pytest knows how to locate that automatically
def test_zone_inert(test_variables):
    """
    no matter how long the simulation is, we should always
    find the zone unchanged if there's no occupants and
    the zone is totally insulated
    """
    # retrieve the complete variables in a Vars instance
    vars          = test_variables
    # define local variables from that set of common 'global-like' 
    volume        = vars.volume
    bob           = vars.bob
    mary          = vars.mary
    insulated     = vars.insulated
    weather       = vars.weather1
    d1            = vars.hourly_2hours
    d2            = vars.hourly_2days
    d3            = vars.hourly_2weeks
    d4            = vars.hourly_2months

    initial = 400
    for date_range in d1, d2, d3, d4:
        zone = Zone(volume, insulated)
        simulation = zone.simulate(date_range, weather, initial)

        assert simulation.co2[-1] == initial
