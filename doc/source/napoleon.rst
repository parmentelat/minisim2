Common docstring styles
=======================

Numpy Style Docstrings Example
----------------------------------------

.. automodule:: doc_samples.numpy
		:members:

Google Style Docstrings Example
----------------------------------------

.. automodule:: doc_samples.google
		:members:
